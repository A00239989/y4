package lab1;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.apache.commons.codec.digest.DigestUtils;

public class Q4 {

	public static void main(String[] args) throws IOException {
		File file = new File("data/text.txt");
		FileInputStream fin = new FileInputStream(file);

		byte[] sha256 = DigestUtils.sha256(fin);

		String encodedDigest = Base64.getEncoder().encodeToString(sha256);

		System.out.println("Base64 encoded message digest " + encodedDigest);

	}
}
