package lab1;

import org.apache.commons.codec.digest.DigestUtils;

public class Q3 {
	public static void main(String[] args) {

		// encodeSHA256(password);
		// encodeSHA224(password);

		String password = "12345";
		String md5 = DigestUtils.md5Hex(password);
		System.out.println("password " + password);
		System.out.println("password " + md5);

		String sha256 = DigestUtils.sha256Hex(password);
		System.out.println("password " + password);
		System.out.println("password " + sha256);

	}
}
