package lab1;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.apache.commons.codec.digest.DigestUtils;

public class Q1 {
	public static void main(String[] args) {
		String password = "12345";
		String password2 = "123456";
		MessageDigest algorithm = null;
		try {
			algorithm = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		algorithm.reset();
		algorithm.update(password.getBytes());
		byte[] messageDigest = algorithm.digest();
		System.out.println("length " + messageDigest.length);

		String encodedDigest = Base64.getEncoder().encodeToString(messageDigest);
		
		System.out.println("Base64 encoded message digest " + encodedDigest);
		
		byte[] messageDigest2 = algorithm.digest(password2.getBytes());
		String encodedDigest2 = Base64.getEncoder().encodeToString(messageDigest2);
		
		System.out.println("Base64 encoded message digest 2 " + encodedDigest2);
		
	}
}
