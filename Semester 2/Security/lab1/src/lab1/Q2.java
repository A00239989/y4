package lab1;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class Q2 {

	public static void main(String[] args) throws IOException {
		File file = new File("data/text.txt");
		FileInputStream fin = new FileInputStream(file);

		MessageDigest algorithm = null;
		try {
			algorithm = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		byte[] buffer = new byte[32];
		int bytesRead = 0;
		
		
		while ((bytesRead = fin.read(buffer)) > 0) {
			algorithm.update(buffer, 0, bytesRead);
			System.out.println(" Number of bytes is " + bytesRead);

		}
		fin.close();
	}

}
