package lab6;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.Signature;

public class lab6Q3 {

	public static void main(String[] args) throws Exception {
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA");

		PublicKey publicKey = (PublicKey) readFromFile("data/lab6PublicKey");
		byte[] sig = (byte[]) readFromFile("data/lab6Digital");
		String sendText = (String) readFromFile("data/lab6Text");

		Signature dsa = Signature.getInstance("SHA1withDSA");
		dsa.initVerify(publicKey);

		byte[] textArray = sendText.getBytes();
		dsa.update(textArray);
		boolean verified = dsa.verify(sig);

		System.out.println("signature verifies: " + verified);

	}

	static Object readFromFile(String filename) throws Exception {
		FileInputStream fin = new FileInputStream(filename);
		ObjectInputStream oin = new ObjectInputStream(fin);
		Object object = oin.readObject();
		oin.close();
		return object;
	}

}
