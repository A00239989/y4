package lab6;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Base64;

public class lab6Q2 {

	public static void main(String[] args) throws Exception {
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA");

		SecureRandom random = new SecureRandom();
		random.setSeed(2345);
		keyGen.initialize(1024, random);

		KeyPair pair = keyGen.generateKeyPair();
		PrivateKey privateKey = pair.getPrivate();
		PublicKey publicKey = pair.getPublic();
		writeToFile("data/lab6PublicKey", publicKey);

		// sending the data
		Signature dsa = Signature.getInstance("SHA1withDSA");
		dsa.initSign(privateKey);
		byte[] sendText = "Sending Data".getBytes();
		dsa.update(sendText);
		byte[] sig = dsa.sign();
		writeToFile("data/lab6Digital", sig);
		writeToFile("data/lab6Text", sendText.toString());

	}

	static void writeToFile(String filename, Object object) throws Exception {
		FileOutputStream fout = new FileOutputStream(filename);
		ObjectOutputStream oout = new ObjectOutputStream(fout);
		oout.writeObject(object);
		oout.close();
	}

}
