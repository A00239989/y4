package lab2;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.SecretKey;

public class lab1Q3 {

	public static void main(String[] args) throws Exception {
		// read secret key
	
		SecretKey sk = (SecretKey) readFromFile("data/secretKey.txt");
		
		String encodedHmac = (String) readFromFile("data/hmac.txt");
		byte[] sentHmac = Base64.getDecoder().decode(encodedHmac);
		String sendText = (String) readFromFile("data/sendText.txt");
		
		// Calculate hmac
		
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(sk);
		byte[] myHmac = mac.doFinal(sendText.getBytes());
		
		System.out.println("Check: " + Arrays.equals(sentHmac, myHmac));

	}

	static Object readFromFile(String filename) throws Exception {
		FileInputStream fin = new FileInputStream(filename);
		ObjectInputStream oin = new ObjectInputStream(fin);
		Object object = oin.readObject();
		oin.close();
		return object;
	}

}
