package lab2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;

public class lab1Q2 {

	public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
		File file = new File("data/secretKey.txt");
		File file2 = new File("data/sendText.txt");
		File file3 = new File("data/hmac.txt");

		// Generate a HmacSha256 Secret Key
		KeyGenerator kg = KeyGenerator.getInstance("HmacSHA256");
		SecretKey sk = kg.generateKey();
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(sk);

		byte[] result = mac.doFinal();

		try (FileOutputStream stream = new FileOutputStream(file)) {
			stream.write(result);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String str = "Hi Fren, do you wanna go out for dinner tommorow in Vienna harbour";
		BufferedWriter writer = new BufferedWriter(new FileWriter(file2));
		writer.write(str);

		writer.close();

		byte[] textArray = str.getBytes();
		Mac mac2 = Mac.getInstance("HmacSHA256");
		mac2.init(sk);
		byte[] hmac = mac2.doFinal(textArray);
		String encodeHmac = Base64.getEncoder().encodeToString(hmac);

		BufferedWriter writer2 = new BufferedWriter(new FileWriter(file3));
		writer2.write(encodeHmac);

		writer2.close();

		System.out.println(hmac.length);
		System.out.println("Base64 encoded message digest: " + encodeHmac);

	}
}
