import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;

@SuppressWarnings("serial")
public class JDBCMainWindowContent extends JInternalFrame implements ActionListener {
	String cmd = null;

	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private Container content;

	private JPanel detailsPanel;
	private JPanel exportButtonPanel;
	// private JPanel exportConceptDataPanel;
	private JScrollPane dbContentsPanel;

	private Border lineBorder;

	private JLabel IDLabel = new JLabel("ID:                 ");
	private JLabel FirstNameLabel = new JLabel("FirstName:               ");
	private JLabel LastNameLabel = new JLabel("LastName:      ");
	private JLabel NickNameLabel = new JLabel("NickName:        ");
	private JLabel GenderLabel = new JLabel("Gender:                 ");
	private JLabel DobLabel = new JLabel("DOB:                 ");
	private JLabel BusinessLabel = new JLabel("Business:                 ");
	private JLabel StatusLabel = new JLabel("Status:               ");
	private JLabel VoicedByLabel = new JLabel("Voiced By:      ");

	private JTextField IDTF = new JTextField(10);
	private JTextField FirstNameTF = new JTextField(10);
	private JTextField LastNameTF = new JTextField(10);
	private JTextField NickNameTF = new JTextField(10);
	private JTextField GenderTF = new JTextField(10);
	private JTextField dobTF = new JTextField(10);
	private JTextField businessTF = new JTextField(10);
	private JTextField StatusTF = new JTextField(10);
	private JTextField VoicedByTF = new JTextField(10);


	private static QueryTableModel TableModel = new QueryTableModel();
	// Add the models to JTabels
	private JTable TableofDBContents = new JTable(TableModel);
	// Buttons for inserting, and updating members
	// also a clear button to clear details panel
	private JButton updateButton = new JButton("Update");
	private JButton insertButton = new JButton("Insert");
	private JButton exportButton = new JButton("Export");
	private JButton deleteButton = new JButton("Delete");
	private JButton clearButton = new JButton("Clear");
	private JButton changeDBButton = new JButton("Change View");

	private JButton ListGenders = new JButton("Number of Female Characters");
	private JButton listDeadCharacters = new JButton("Number of Dead Characters");
	private JButton characterOccurances_sa_vice = new JButton("Characters in GTA SA & GTA VICE");
	private JButton characterOccurances_sa_three = new JButton("Characters in GTA SA & GTA 3");

	private String currentlyBrowsing = "gta_sa";

	public JDBCMainWindowContent(String aTitle) {
		// setting up the GUI
		super(aTitle, false, false, false, false);
		setEnabled(true);

		initiate_db_conn();
		// add the 'main' panel to the Internal Frame
		content = getContentPane();
		content.setLayout(null);
		content.setBackground(Color.lightGray);
		lineBorder = BorderFactory.createEtchedBorder(15, Color.red, Color.black);

		// setup details panel and add the components to it
		detailsPanel = new JPanel();
		detailsPanel.setLayout(new GridLayout(11, 2));
		detailsPanel.setBackground(Color.lightGray);
		detailsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "CRUD Actions"));

		detailsPanel.add(IDLabel);
		detailsPanel.add(IDTF);
		detailsPanel.add(FirstNameLabel);
		detailsPanel.add(FirstNameTF);
		detailsPanel.add(LastNameLabel);
		detailsPanel.add(LastNameTF);
		detailsPanel.add(NickNameLabel);
		detailsPanel.add(NickNameTF);
		detailsPanel.add(GenderLabel);
		detailsPanel.add(GenderTF);
		detailsPanel.add(DobLabel);
		detailsPanel.add(dobTF);
		detailsPanel.add(BusinessLabel);
		detailsPanel.add(businessTF);
		detailsPanel.add(StatusLabel);
		detailsPanel.add(StatusTF);
		detailsPanel.add(VoicedByLabel);
		detailsPanel.add(VoicedByTF);

		// setup details panel and add the components to it
		exportButtonPanel = new JPanel();
		exportButtonPanel.setLayout(new GridLayout(2, 2));
		exportButtonPanel.setBackground(Color.lightGray);
		exportButtonPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Export Data"));
		exportButtonPanel.add(ListGenders);
		exportButtonPanel.add(listDeadCharacters);
		exportButtonPanel.add(characterOccurances_sa_vice);
		exportButtonPanel.add(characterOccurances_sa_three);

		exportButtonPanel.setSize(500, 200);
		exportButtonPanel.setLocation(3, 300);
		content.add(exportButtonPanel);

		insertButton.setSize(100, 30);
		updateButton.setSize(100, 30);
		exportButton.setSize(100, 30);
		deleteButton.setSize(100, 30);
		clearButton.setSize(100, 30);
		changeDBButton.setSize(110, 30);

		insertButton.setLocation(370, 10);
		updateButton.setLocation(370, 110);
		exportButton.setLocation(370, 160);
		deleteButton.setLocation(370, 60);
		clearButton.setLocation(370, 210);
		changeDBButton.setLocation(365, 250);

		insertButton.addActionListener(this);
		updateButton.addActionListener(this);
		exportButton.addActionListener(this);
		deleteButton.addActionListener(this);
		clearButton.addActionListener(this);
		changeDBButton.addActionListener(this);

		this.ListGenders.addActionListener(this);
		this.listDeadCharacters.addActionListener(this);
		this.characterOccurances_sa_vice.addActionListener(this);
		this.characterOccurances_sa_three.addActionListener(this);
	

		content.add(insertButton);
		content.add(updateButton);
		content.add(exportButton);
		content.add(deleteButton);
		content.add(clearButton);
		content.add(changeDBButton);

		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(900, 300));

		dbContentsPanel = new JScrollPane(TableofDBContents, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		dbContentsPanel.setBackground(Color.lightGray);
		dbContentsPanel
				.setBorder(BorderFactory.createTitledBorder(lineBorder, "Currently browsing: Grand Theft Auto"));

		detailsPanel.setSize(360, 300);
		detailsPanel.setLocation(3, 0);
		dbContentsPanel.setSize(700, 300);
		dbContentsPanel.setLocation(477, 0);

		content.add(detailsPanel);
		content.add(dbContentsPanel);

		setSize(982, 645);
		setVisible(true);

		TableModel.refreshFromDB(stmt, currentlyBrowsing);
	}

	public void initiate_db_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/JDBC_Assignment";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "root");
			// Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
		}
	}

	// event handling
	public void actionPerformed(ActionEvent e) {
		Object target = e.getSource();
		if (target == clearButton) {
			IDTF.setText("");
			FirstNameTF.setText("");
			LastNameTF.setText("");
			NickNameTF.setText("");
			GenderTF.setText("");
			dobTF.setText("");
			businessTF.setText("");
			GenderTF.setText("");
			StatusTF.setText("");
			VoicedByTF.setText("");
		

		}
		if (target == changeDBButton) {
			String SELECTALL = "";

			try {
				if (currentlyBrowsing.equals("gta_sa")) {
					SELECTALL = "SELECT * from gta_vice";
					currentlyBrowsing = "gta_vice";
				} else if (currentlyBrowsing.equals("gta_vice")) {
					SELECTALL = "SELECT * from gta_three";
					currentlyBrowsing = "gta_three";
				} else if (currentlyBrowsing.equals("gta_three")) {
					SELECTALL = "SELECT * from gta_sa";
					currentlyBrowsing = "gta_sa";
				}
				stmt.executeQuery(SELECTALL);

			} catch (SQLException sqle) {
				System.err.println("Error with  insert:\n" + sqle.toString());
			} finally {
				TableModel.refreshFromDB(stmt, currentlyBrowsing);
			}

		}

		if (target == insertButton) 
			try {
				String updateTemp = "INSERT INTO " + currentlyBrowsing + " VALUES(" + null + ",'" + FirstNameTF.getText() + "','"
						+ LastNameTF.getText() + "','" + NickNameTF.getText() + "','" + GenderTF.getText() + "','" + dobTF.getText() + "','" + businessTF.getText() + "','"
						+ StatusTF.getText() + "','" + VoicedByTF.getText() + "');";
				System.out.println(updateTemp);

				stmt.executeUpdate(updateTemp);

			} catch (SQLException sqle) {
				System.err.println("Error with  insert:\n" + sqle.toString());
			} finally {
				TableModel.refreshFromDB(stmt, currentlyBrowsing);
			}
		
		if (target == deleteButton) {

			try {
				String updateTemp = "DELETE FROM " + currentlyBrowsing + "  WHERE id = " + IDTF.getText() + ";";
				stmt.executeUpdate(updateTemp);

			} catch (SQLException sqle) {
				System.err.println("Error with delete:\n" + sqle.toString());
			} finally {
				TableModel.refreshFromDB(stmt, currentlyBrowsing);
			}
		}
		if (target == updateButton) {
			try {

				String updateTemp = "";
				if(currentlyBrowsing.equals("gta_sa"))
				{
					 updateTemp = "call SA_update_characters(" + IDTF.getText() + ",' " + FirstNameTF.getText() + "', '" + LastNameTF.getText() + "', '"
							+ NickNameTF.getText() + "','" + GenderTF.getText() + "', '" + dobTF.getText() + "', '" + businessTF.getText() + "', '" + StatusTF.getText() + "',' " + VoicedByTF.getText() + "');";
				}
				else if(currentlyBrowsing.equals("gta_vice"))
				{
					 updateTemp = "call VICE_update_characters(" + IDTF.getText() + ",' " + FirstNameTF.getText() + "', '" + LastNameTF.getText() + "', '"
							 + NickNameTF.getText() + "','" + GenderTF.getText() + "', '" + dobTF.getText() + "', '" + businessTF.getText() + "', '" + StatusTF.getText() + "',' " + VoicedByTF.getText() + "');";
				}
				else if(currentlyBrowsing.equals("gta_three"))
				{
					 updateTemp = "call Three_update_characters(" + IDTF.getText() + ",' " + FirstNameTF.getText() + "', '" + LastNameTF.getText() + "', '"
							 + NickNameTF.getText() + "','" + GenderTF.getText() + "', '" + dobTF.getText() + "', '" + businessTF.getText() + "', '" + StatusTF.getText() + "',' " + VoicedByTF.getText() + "');";
				}
				
				
				

				System.out.println(updateTemp);

				stmt.executeUpdate(updateTemp);
				// these lines do nothing but the table updates when we access the db.
				rs = stmt.executeQuery("SELECT * from " + currentlyBrowsing  +"");
				rs.next();
				rs.close();
			} catch (SQLException sqle) {
				System.err.println("Error with  update:\n" + sqle.toString());
			} finally {
				TableModel.refreshFromDB(stmt, currentlyBrowsing);
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////
		// I have only added functionality of 2 of the button on the lower right of the
		///////////////////////////////////////////////////////////////////////////////////// template
		///////////////////////////////////////////////////////////////////////////////////

		if (target == this.ListGenders) {

			cmd = "select gender as NoOfMalesVsFemales  from gta_sa union all select gender from gta_vice union all select gender from gta_three;";
			
			String cmd2 = "select (SELECT count(*) from gta_sa where gender = 'F') as No_Of_Females_GTA_SA,(SELECT count(*) from gta_vice where gender = 'F') as No_Of_Females_GTA_VICE, (SELECT count(*) from gta_three where gender = 'F') as No_Of_Females_GTA_THREE";

			String cmd3 = "select (SELECT count(*) from gta_sa where status = 'Alive') as GTA_SA,(SELECT count(*) from gta_vice where status = 'Alive') as GTA_VICE,(SELECT count(*) from gta_three where status = 'Alive') as GTA_THREE";
			
			try {
				rs = stmt.executeQuery(cmd2);
				writeToFile(rs, ListGenders.getText());
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		}
		if (target == this.listDeadCharacters) {

			//cmd = "select gender as NoOfMalesVsFemales  from gta_sa union all select gender from gta_vice union all select gender from gta_three;";
			
			

			String cmd3 = "select (SELECT count(*) from gta_sa where status = 'Alive') as No_Of_Dead_GTA_SA,(SELECT count(*) from gta_vice where status = 'Alive') as No_Of_Dead_GTA_VICE,(SELECT count(*) from gta_three where status = 'Alive') as No_Of_Dead_GTA_THREE";
			
			try {
				rs = stmt.executeQuery(cmd3);
				writeToFile(rs, listDeadCharacters.getText());
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		}
		if (target == this.exportButton) {

			cmd = "select * from " + currentlyBrowsing + " ;";
			
			try {
				rs = stmt.executeQuery(cmd);
				writeToFile(rs, currentlyBrowsing);
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		}
		if (target == this.characterOccurances_sa_vice) {

			cmd = "SELECT firstName, lastName, gender,business FROM gta_sa as b WHERE EXISTS ( SELECT * FROM gta_vice as a WHERE b.firstName = a.firstName AND a.lastName = b.lastName AND a.nickName = b.nickName);";
			
			try {
				rs = stmt.executeQuery(cmd);
				writeToFile(rs, characterOccurances_sa_vice.getText());
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		}
		if (target == this.characterOccurances_sa_three) {

			cmd = "SELECT firstName, lastName, gender,business FROM gta_sa as b WHERE EXISTS ( SELECT * FROM gta_three as a WHERE b.firstName = a.firstName AND a.lastName = b.lastName AND a.nickName = b.nickName);";
			
			try {
				rs = stmt.executeQuery(cmd);
				writeToFile(rs, characterOccurances_sa_three.getText());
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		}


		
		
	}
	///////////////////////////////////////////////////////////////////////////

	private void writeToFile(ResultSet rs, String browsing) {
		try {
			System.out.println("In writeToFile");
			FileWriter outputFile = new FileWriter("Results for " + browsing + ".csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for (int i = 0; i < numColumns; i++) {
				printWriter.print(rsmd.getColumnLabel(i + 1) + ",");
			}
			printWriter.print("\n");
			while (rs.next()) {
				for (int i = 0; i < numColumns; i++) {
					printWriter.print(rs.getString(i + 1) + ",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
