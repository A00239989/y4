Thesis Readme:
05/11/2019:
Writing up an Introduction of Thesis write up, which consists of description of the problem faced by the IT world, providing description of the intented work of the 
application at a high level.

15/11/2019:	
Created a SWING Interface for Road Sign Recognition using netbeans. The UI consists of labels, textfields for amount of signs recognised and buttons such as
save current route. Installed OpenCV as a computer vision library on my PC, adding it as a User Library to the current project.
More reseach on Haar vs Yolo, also display functionality of openCV

19/11/2019:
Attempt at creating a VideoCapture object of OpenCV library, adding a mp4 sample video and playing it in the background. The video plays succesfully in the background.
Plan to 
Research on OpenCV using gitHub repositories such as:
https://github.com/aarcosg/TSD-OpenCV-Java

25/11/2019
Using haarCascades found at link below:
https://github.com/cfizette/road-sign-cascades/blob/master/Yield%20Signs/Yield%20Sign%20LBP/yieldsign12Stages.xml

Attempting at connecting VideoCapture created below providing it with Haar Cascades in order to spot road signs. Issues with connecting the VideoCapture with interface created.
The application now is able to spot yield road signs, but the application is split into two classess which are not connected. Hence, interface vs recognition class.

30/11/2019
Re-structuring the interface, adding the VideoCapture functionality as a JLabel into the JPanel destinated to playback captured video. Issues with re-sizing the VideoCapture object 
which displays the video. VideoCapture only displays first frame of the video.

02/12/2019
Another re-structure of the interface, problem with the first frame has being fixed. Adding a basic recognition algorithm for the presentation. The application now displays
playback within the interface, notifies in eclipse command if it has spotted a yield sign.