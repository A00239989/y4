import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;

public class Main {

	static {
		try {
			System.load("C:\\Users\\Mateusz\\Downloads\\opencv\\build\\x64\\vc14\\bin\\opencv_ffmpeg410_64.dll");
		} catch (UnsatisfiedLinkError e) {
			System.err.println("Native code library failed to load.\n" + e);
			System.exit(1);
		}
	}
	public static VideoCapture camera = null;
	public static Mat frame = null;
	public static int noOfDetections = 0;

	public static void main(String[] args) {
	
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		frame = new Mat();
		camera = new VideoCapture("C:\\Users\\Mateusz\\Downloads\\preview.mp4");

		initInterface();
	}

	public static void initInterface() {
		JFrame mainFrame = new JFrame("Road Sign Recognition");
		JPanel footerPanel = new JPanel();
		JPanel headerPanel = new JPanel();
		JButton saveRouteButton = new JButton();
		JButton deleteRouteButton = new JButton();
		JButton jButton3 = new JButton();
		JButton jButton4 = new JButton();
		JLabel TimeLabel = new JLabel();
		JLabel dateLabel = new JLabel();
		JLabel lengthLabel = new JLabel();
		JLabel SpeedSignCapturedLabel = new JLabel();
		JLabel warningSignsCaptured = new JLabel();
		JLabel otherSignsCapturedLabel = new JLabel();
		JLabel currentSpeedLabel = new JLabel();
		JLabel legalSpeedLabel = new JLabel();
		JLabel speedRatioLabel = new JLabel();
		JMenu jMenu1 = new JMenu();
		JMenu jMenu2 = new JMenu();
		JMenuBar jMenuBar1 = new JMenuBar();
		JPanel jPanel1 = new JPanel();
		JPanel jPanel2 = new JPanel();
		JTextField jTextField1 = new JTextField();
		JTextField jTextField2 = new JTextField();
		JTextField jTextField3 = new JTextField();
		JTextField jTextField4 = new JTextField();
		JTextField jTextField5 = new JTextField();
		JTextField jTextField6 = new JTextField();
		JTextField jTextField7 = new JTextField();
		JTextField jTextField8 = new JTextField();
		JTextField jTextField9 = new JTextField();
		JMenu resetMenu = new JMenu();
		JPanel sidePanel = new JPanel();
		JLabel videoContainer = new JLabel();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		mainFrame.setTitle("Traffic Sign Recognition ");
		mainFrame.getContentPane().setLayout(new java.awt.BorderLayout(2, 2));

		headerPanel.setLayout(new java.awt.GridLayout(2, 4));

		TimeLabel.setText("Time");
		headerPanel.add(TimeLabel);

		dateLabel.setText("Date");
		headerPanel.add(dateLabel);

		lengthLabel.setText("Lenght of Route");
		headerPanel.add(lengthLabel);

		jTextField1.setText("Placeholder");
		headerPanel.add(jTextField1);

		jTextField2.setText("Placeholder");
		headerPanel.add(jTextField2);

		jTextField3.setText("Placeholder");
		headerPanel.add(jTextField3);

		mainFrame.add(headerPanel, java.awt.BorderLayout.PAGE_START);

		sidePanel.setLayout(new java.awt.GridLayout(6, 3));

		SpeedSignCapturedLabel.setText("No. of speed  signs captured");
		sidePanel.add(SpeedSignCapturedLabel);

		jTextField4.setText("Placeholder");
		sidePanel.add(jTextField4);

		warningSignsCaptured.setText("No. of warning signs captured");
		sidePanel.add(warningSignsCaptured);

		jTextField5.setText("Placeholder");
		sidePanel.add(jTextField5);

		otherSignsCapturedLabel.setText("no. of other signs captured");
		sidePanel.add(otherSignsCapturedLabel);

		jTextField6.setText("Placeholder");
		sidePanel.add(jTextField6);

		mainFrame.add(sidePanel, java.awt.BorderLayout.LINE_START);

		saveRouteButton.setText("Save this route");

		footerPanel.add(saveRouteButton);

		deleteRouteButton.setText("Delete this Route");
		footerPanel.add(deleteRouteButton);

		jButton3.setText("Sample Button");
		footerPanel.add(jButton3);

		jButton4.setText("Sample Button");
		footerPanel.add(jButton4);

		mainFrame.add(footerPanel, java.awt.BorderLayout.PAGE_END);

		jPanel1.setLayout(new java.awt.GridLayout(6, 2));

		currentSpeedLabel.setText("Current Speed");
		jPanel1.add(currentSpeedLabel);

		jTextField7.setText("Placeholder");
		jPanel1.add(jTextField7);

		legalSpeedLabel.setText("Legal Speed");
		jPanel1.add(legalSpeedLabel);

		jTextField8.setText("Placeholder");
		jPanel1.add(jTextField8);

		speedRatioLabel.setText("Speed ratio");
		jPanel1.add(speedRatioLabel);

		jTextField9.setText("Placeholder");
		jPanel1.add(jTextField9);

		mainFrame.add(jPanel1, java.awt.BorderLayout.LINE_END);

		mainFrame.add(videoContainer, java.awt.BorderLayout.CENTER);

		jMenu1.setText("File");
		jMenuBar1.add(jMenu1);

		jMenu2.setText("Edit");
		jMenuBar1.add(jMenu2);

		resetMenu.setText("Reset");
		jMenuBar1.add(resetMenu);

		mainFrame.setJMenuBar(jMenuBar1);

		mainFrame.setSize(800, 600);
		jPanel2.setSize(400, 400);

		mainFrame.setVisible(true);

		while (true) {
			if (camera.read(frame)) {
				Size sz = new Size(1200, 1000);
				Imgproc.resize(frame, frame, sz);

				frame = detectCars(frame);
				ImageIcon image = new ImageIcon(Mat2bufferedImage(frame));
				videoContainer.setIcon(image);
				videoContainer.repaint();

			}
		}
	}

	private static BufferedImage Mat2bufferedImage(Mat image) {
		MatOfByte bytemat = new MatOfByte();
		Imgcodecs.imencode(".jpg", image, bytemat);
		byte[] bytes = bytemat.toArray();
		InputStream in = new ByteArrayInputStream(bytes);
		BufferedImage img = null;
		try {
			img = ImageIO.read(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return img;
	}

	public static Mat detectCars(Mat image) {


		CascadeClassifier yieldSignCascade = new CascadeClassifier("C:\\Users\\Mateusz\\Desktop\\cascades.xml");

		MatOfRect roadDetections = new MatOfRect();
		yieldSignCascade.detectMultiScale(image, roadDetections);

		//System.out.println(String.format("%s Yield Signs Detected", roadDetections.toArray().length));

		for (Rect rect : roadDetections.toArray()) {
			Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
					new Scalar(0, 200, 0));
			
			System.out.println("A sign was detected");

			
	
		}
		return image;
	}

}
